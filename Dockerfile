##FROM gcr.io/webinar-demo-project-270214/maven-jdk-11-with-dependencies:1.0.0 AS build
##COPY src /usr/src/app/src
##COPY pom.xml /usr/src/app
##RUN mvn -f /usr/src/app/pom.xml clean package -Dmaven.test.skip=true

##FROM openjdk:11
##COPY --from=build /usr/src/app/target/gcp-webinar-app-1.0.0.jar /usr/app/gcp-webinar-app-1.0.0.jar
##EXPOSE 8080
##ENTRYPOINT ["java","-jar","/usr/app/gcp-webinar-app-1.0.0.jar"]

FROM openjdk:16-slim
COPY target/beanstalk.jar /usr/app/beanstalk.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/usr/app/beanstalk.jar"]
